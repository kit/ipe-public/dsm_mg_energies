import pandas as pd
import numpy as np
from datetime import datetime

from openpyxl import load_workbook


class ConsumerData:
    def __init__(self, file):
        # Start and end of scheduling
        self.scheduling_start = 0
        self.scheduling_end = 8760

        self.consumers_list_df = pd.DataFrame([])
        self.scheduled_consumers_data = {}  # Dict to hold all data of scheduled consumers

        self.read_consumers_data(file)  # Read consumers data on init



    def read_consumers_data(self, filename):
        """
        Reads consumer data Excel file and saves list of consumers as dataframe and dict containing dataframe with
        operation data of each scheduled consumer in self.consumer_data
        :param filename:
        :return:
        """

        consumer_data = load_workbook(filename=filename, read_only=False, keep_vba=False, data_only=True, keep_links=False)

        self.consumers_list_df = pd.read_excel(io=filename, sheet_name='consumer_list', skiprows=1, engine='openpyxl')

        # Get list of all scheduled consumers
        scheduled_consumers = self.consumers_list_df.loc[self.consumers_list_df['Scheduled'] == 'yes']['ID'].tolist()

        # Get scheduling configuration data
        self.scheduling_configuration = \
            pd.read_excel(io=filename, sheet_name='configuration', skiprows=0, engine='openpyxl')

        # Get hour of year for scheduling start and end from datetime
        ui_scheduling_start = self.scheduling_configuration.loc[
            self.scheduling_configuration.parameter == 'scheduling_start_datetime', 'value'].item()
        ui_scheduling_end = self.scheduling_configuration.loc[
            self.scheduling_configuration.parameter == 'scheduling_end_datetime', 'value'].item()

        # If scheduling start and end is specified -> change configuration from default (full year)
        if not pd.isnull(ui_scheduling_start) and not pd.isnull(ui_scheduling_end):
            # Get hour of year for scheduling start and end from datetime
            beginning_of_year = datetime(ui_scheduling_start.year, 1, 1, tzinfo=ui_scheduling_start.tzinfo)

            self.scheduling_start = int((ui_scheduling_start - beginning_of_year).total_seconds() // 3600)
            self.scheduling_end = int((ui_scheduling_end - beginning_of_year).total_seconds() // 3600)

        # Create dict to hold scheduled consumer's data

        for consumer in scheduled_consumers:  # Loop through all scheduled consumers
            data = consumer_data[consumer].values
            cols = next(data)
            data = list(data)

            # Return dict entry for every worksheet
            self.scheduled_consumers_data[consumer] = {
                'operating_days': pd.DataFrame(data, columns=cols)[['Operating days', 'Daily operation']].dropna(),
                'operating_hours': pd.DataFrame(data, columns=cols)[['Operating hours', 'Hourly operation']].dropna(),
                'holidays': pd.DataFrame(data, columns=cols)[['Holidays', 'Date', 'Holiday operation']].dropna(),
                'operating_blocks': pd.DataFrame(data, columns=cols)[['Operating blocks', 'Release', 'Deadline',
                                                                      'Duration', 'Power']]
                    .dropna(subset=['Operating blocks']).fillna(value={'Days to following': 0})

            }


class Consumer:
    def __init__(self, consumer_id, consumer_name, scheduled_consumer_data):

        self.consumer_id = consumer_id  # Int ID starting at 0, specific for every consumer!
        self.consumer_name = consumer_name
        self.scheduled_consumer_data = scheduled_consumer_data

        # Dataframe to store this consumer's operation time. Column: [Operation (int: 1 to 3)]
        self.operation_times = pd.DataFrame([])

        # Dataframe to store this consumer's operation blocks.
        self.operation_blocks = pd.DataFrame([])

    def define_operation_times(self):
        """
        Creates dataframe with row for every hour of the week. Containing column "Operation" which gives the possible
        operation of this consumer for every hour. (1 - preferred, 2 - possible, 3 - blocked)
        Uses: self.scheduled_consumer_data (dict)
        Writes result in: self.operation_times (list)
        :return:
        """

        # Create hourly operation for one week

        # List to hold hourly operation for one week.
        operation_week = []
        for day_index, day in self.scheduled_consumer_data['operating_days'].iterrows():  # Loop through days of the week.

            if day['Daily operation'] == 1:  # If day is preferred.
                # Add preferred hours as given. Turn float values to int
                operation_week.extend(self.scheduled_consumer_data['operating_hours']['Hourly operation'].
                                      astype(int).values.tolist())

            elif day['Daily operation'] == 3:  # If day is blocked.
                operation_week.extend(24*[3])  # Add 24 blocked hours

            elif day['Daily operation'] == 2:  # If day is possible -> "downgrade" preferred hours to "possible"
                # Loop through every hour
                for hour_index, hour in self.scheduled_consumer_data['operating_hours'].iterrows():
                    if hour['Hourly operation'] == 1:  # If preferred operation hour.
                        operation_week.append(2)  # "Downgrade" to possible operation hour.
                    elif hour['Hourly operation'] == 2:  # If possible operation hour.
                        operation_week.append(2)  # Keep as possible operation hour.
                    elif hour['Hourly operation'] == 3:  # If blocked operation hour.
                        operation_week.append(3)  # Keep as blocked operation hour.

        # Extend one week operation to the full year.
        operation_year = operation_week * 53
        # Cut to one year (8760 hours).
        operation_year = operation_year[:365*24]

        # Add holidays
        for holiday_index, holiday in self.scheduled_consumer_data['holidays'].iterrows():  # Loop through holidays in list
            # Turn date from Excel into string and make year 2019 -> Avoid leap years.
            # 2019 also starts on a Monday -> Consistent with the operation_year here.
            # No holiday can be added on a 29th of February!
            holiday_date = '2019' + str(holiday['Date'])[4:]

            # Calculate the number of the day of the year of the given holiday. Subtract 1 to start day count at 0.
            holiday_date_number = datetime.fromisoformat(holiday_date).timetuple().tm_yday - 1

            # Change holiday operation in operation_year list.
            hour = start_hour = holiday_date_number * 24
            while hour <= (start_hour + 23):  # Loop through the hours of this holiday in operation_year list.
                if holiday['Holiday operation'] == 2:  # If operation is possible on this holiday.
                    if operation_year[hour] == 1:  # If preferred operation hour.
                        operation_year[hour] = 2  # "Downgrade" to possible operation hour.
                    # No changes to possible or blocked hours

                elif holiday['Holiday operation'] == 3:  # If operation is blocked on this holiday.
                    operation_year[hour] = 3   # "Downgrade" all of this day's hours to blocked operation hour.

                hour = hour + 1  # Increment hour counter.

        # Save operation year array
        self.operation_times = np.array(operation_year)

        return

    def define_operation_blocks(self):
        """
        Create operation blocks dataframe
        :return:
        """

        # Assign self.operation_blocs
        self.operation_blocks = self.scheduled_consumer_data['operating_blocks']

        def date_to_int(date):
            try:
                # Turn date into day of the year starting at 0
                return datetime.fromisoformat('2019' + str(date)[4:]).timetuple().tm_yday - 1
            except:
                # If no date given, enter 'none'.
                return 'none'

        # Turn deadline and release dates into int (day count for year)
        self.operation_blocks['Release'] = self.operation_blocks['Release'].map(date_to_int)
        self.operation_blocks['Deadline'] = self.operation_blocks['Deadline'].map(date_to_int)





