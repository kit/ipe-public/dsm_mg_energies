import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import cm
import matplotlib.ticker
import calendar
import UI_deap_dsm_config
import numpy as np

from matplotlib.lines import Line2D

# ---- Set matplotlib style ----

SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=MEDIUM_SIZE)  # fontsize of the figure title

full_width = 5.45776
small_height = 2
medium_height = 4
large_height = 6


def plot_lcoe_flexibility_bar_chart(scenarios_list):

    possible_hours = []
    blocked_hours = []
    scenarios_lcoe_min = []
    scenarios_lcoe_mid = []
    scenarios_lcoe_max = []

    scenarios_list = scenarios_list[::-1]  # reverse list

    labels = []
    for scenario in scenarios_list:
        # Add scenario's flexibility costs in two lists
        if scenario.best_dsm_result:
            possible_hours.append(scenario.best_dsm_result['possible_hours'])
            blocked_hours.append(scenario.best_dsm_result['blocked_hours'])
        else:  # Benchmark scenarios without dsm result
            possible_hours.append(0)
            blocked_hours.append(0)

        if len(scenario.lcoe) == 3:  # Add scenario's lcoe (min, mid, max) in list
            scenarios_lcoe_min.append(scenario.lcoe[0])
            scenarios_lcoe_mid.append(scenario.lcoe[1])
            scenarios_lcoe_max.append(scenario.lcoe[2])
        else:  # if only one LCOE value -> make "error range" zero -> min, mid, max the same
            scenarios_lcoe_min.append(scenario.lcoe[0])
            scenarios_lcoe_mid.append(scenario.lcoe[0])
            scenarios_lcoe_max.append(scenario.lcoe[0])

        # Generate labels
        #labels.append('PV: ' + str(scenario.pv_system.nominal_power) + ' kWp - Bat: ' + str(scenario.battery.max_soc) + ' kWh')
        labels.append(str(scenario.battery.max_soc) + '  ')

    x_pos = list(range(len(scenarios_list)))  # Position of scenario: 0, 1, 2, 3....

    fig, ax2 = plt.subplots()

    ax1 = ax2.twinx()  # create secondary axis

    # -- Flexibility cost bars
    width = 0.8  # width of bars

    ax2.bar(x_pos, possible_hours, width, label='Unfavored', alpha=0.8)
    ax2.bar(x_pos, blocked_hours, width, bottom=possible_hours, label='Highly unfavored', alpha=0.8)

    ax2.set_ylabel('Required consumer flexibility [h]')
    ax2.set_xticks(x_pos)
    ax2.set_xticklabels(labels)

    fig.autofmt_xdate(bottom=0.2, rotation=90, ha='center')



    # -- LCOE plot --

    ax1.plot(x_pos, scenarios_lcoe_mid, linestyle='dotted', color='#cc0d10ff', label='LCOE', marker='o')
    ax1.fill_between(x_pos, scenarios_lcoe_min, scenarios_lcoe_max,
                     alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848', linestyle='dotted', antialiased=True
                     )

    ax1.set_ylabel('LCOE [USD/kWh]')

    # Create one common legend
    lines, labels = ax1.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()
    ax2.legend(lines + lines2, labels + labels2, loc='lower left')

    # align y_ticks
    nticks = 22
    ax1.yaxis.set_major_locator(matplotlib.ticker.LinearLocator(nticks))
    ax2.yaxis.set_major_locator(matplotlib.ticker.LinearLocator(nticks))

    # set limits
    ax1.set_ylim([0.14, 0.35])
    ax2.set_ylim([0, 4200])
    # Add grid
    ax1.grid()

    fig.tight_layout()
    fig.set_size_inches(full_width, medium_height)
    plt.show()


def plot_lcoe_flexibility_line_chart(scenarios_list):

    possible_hours = []
    blocked_hours = []
    scenarios_lcoe_min = []
    scenarios_lcoe_mid = []
    scenarios_lcoe_max = []

    scenarios_list = scenarios_list[::-1]  # reverse list

    labels = []
    for scenario in scenarios_list:
        # Add scenario's flexibility costs in two lists
        if scenario.best_dsm_result:
            possible_hours.append(scenario.best_dsm_result['possible_hours'])
            blocked_hours.append(scenario.best_dsm_result['blocked_hours'])
        else:  # Benchmark scenarios without dsm result
            possible_hours.append(0)
            blocked_hours.append(0)

        if len(scenario.lcoe) == 3:  # Add scenario's lcoe (min, mid, max) in list
            scenarios_lcoe_min.append(scenario.lcoe[0])
            scenarios_lcoe_mid.append(scenario.lcoe[1])
            scenarios_lcoe_max.append(scenario.lcoe[2])
        else:  # if only one LCOE value -> make "error range" zero -> min, mid, max the same
            scenarios_lcoe_min.append(scenario.lcoe[0])
            scenarios_lcoe_mid.append(scenario.lcoe[0])
            scenarios_lcoe_max.append(scenario.lcoe[0])

        # Generate labels
        labels.append(str(scenario.battery.max_soc))

    x_pos = list(range(len(scenarios_list)))  # Position of scenario: 0, 1, 2, 3....

    fig, ax1 = plt.subplots()
    fig.set_size_inches(full_width, large_height)
    ax1.set_xlim(x_pos[0], x_pos[-1])
    ax1.set_ylabel('LCOE [USD/kWh]')
    ax2 = ax1.twinx()  # create secondary axis

    # -- Flexibility cost line chart --
    flexibility_cost = UI_deap_dsm_config.blocked_hours_objective * np.array(blocked_hours) + \
                       UI_deap_dsm_config.possible_hours_objective * np.array(possible_hours)
    ax2.plot(x_pos, flexibility_cost, linestyle='solid', color='#1b91cc', label='Required consumer flexibility', marker='o')

    ax2.set_ylabel('Required consumer flexibility')
    ax2.set_xticks(x_pos)
    ax2.set_xticklabels(labels)
    fig.autofmt_xdate(bottom=0.2, rotation=90, ha='center')
    ax2.legend(loc='lower right')
    # ax1.grid()

    # -- LCOE plot

    ax1.plot(x_pos, scenarios_lcoe_mid, linestyle='dotted', color='#CC4F1B', label='LCOE', marker='o')
    ax1.fill_between(x_pos, scenarios_lcoe_min, scenarios_lcoe_max,
                     alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848', linestyle='dotted', antialiased=True
                     )

    # Create one common legend
    lines, labels = ax1.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()
    ax2.legend(lines + lines2, labels + labels2, loc='lower left')

    # align y_ticks
    nticks = 22
    ax1.yaxis.set_major_locator(matplotlib.ticker.LinearLocator(nticks))
    ax2.yaxis.set_major_locator(matplotlib.ticker.LinearLocator(nticks))

    # set limits
    ax1.set_ylim([0.14, 0.35])
    ax2.set_ylim([1200, 5400])

    # Add grid
    ax1.grid()

    plt.show()


def plot_weekly_demand_curves(scenarios_list):
    fig, ax = plt.subplots()

    fig.set_size_inches(full_width, medium_height)

    # Get color gradient to plot individual curves
    color = iter(cm.coolwarm(np.linspace(0, 1, len(scenarios_list))))

    # Set ticks and ticklabes on x-axis
    x_ticks = list(range(0, 168, 12))
    ax.set_xlim([0, 168])
    ax.set_xticks(x_ticks)
    days = ['Sun', 'Mo', 'Tue', 'Wed', 'Thu', 'Fr', 'Sat']
    x_ticklabels = []
    for day in days:
        x_ticklabels.extend(['0', '12\n'+day])
    ax.set_xticklabels(x_ticklabels)

    # Create legend
    legend = []
    for scenario in scenarios_list:
        if scenario.mhp.nominal_flow_rate == 45 and scenario.pv_system.nominal_power == 0 and \
                scenario.battery.max_soc == 0:
            c = next(color)
            ax.plot(scenario.weeklong_hourly_mean_demand)
            legend.append('MHP: ' + str(scenario.mhp.nominal_flow_rate) +
                          'l/s - PV: ' + str(scenario.pv_system.nominal_power) + ' kWp - Bat: ' +
                          str(scenario.battery.max_soc) + ' kWh')
        elif scenario.mhp.nominal_flow_rate == 45 and scenario.pv_system.nominal_power == 60 and \
                scenario.battery.max_soc == 60:
            c = next(color)
            ax.plot(scenario.weeklong_hourly_mean_demand)
            legend.append('MHP: ' + str(scenario.mhp.nominal_flow_rate) +
                          'l/s - PV: ' + str(scenario.pv_system.nominal_power) + ' kWp - Bat: ' +
                          str(scenario.battery.max_soc) + ' kWh')

        elif scenario.mhp.nominal_flow_rate == 75 and scenario.pv_system.nominal_power == 60 and \
                scenario.battery.max_soc == 60:
            c = next(color)
            ax.plot(scenario.weeklong_hourly_mean_demand)
            legend.append('MHP: ' + str(scenario.mhp.nominal_flow_rate) +
                          'l/s - PV: ' + str(scenario.pv_system.nominal_power) + ' kWp - Bat: ' +
                          str(scenario.battery.max_soc) + ' kWh')

        elif scenario.mhp.nominal_flow_rate == 45 and scenario.pv_system.nominal_power == 87.1:
            c = next(color)

            first_part = scenario.weeklong_hourly_mean_demand.tail(96).to_numpy()
            attach = scenario.weeklong_hourly_mean_demand.head(72).to_numpy()

            week = np.concatenate((first_part, attach))

            ax.plot(week, drawstyle='steps')
            legend.append('Homer average load profile')


    ax.set_xlabel("Time [h] and weekdays")
    ax.set_ylabel("Power demand [kW]")

    # Shrink current axis's height by 10% on the bottom
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1,
                     box.width, box.height * 0.9])

    # Put a legend below current axis
    ax.legend(legend, loc='upper center', bbox_to_anchor=(0.5, 1.2),
              fancybox=True, ncol=2)

    plt.grid()
    plt.show()


def plot_weekly_flexibility_demand(scenarios_list):
    fig, ax = plt.subplots()

    fig.set_size_inches(full_width, medium_height)

    # Set ticks and ticklabes on x-axis
    x_ticks = list(range(0, 168, 12))
    ax.set_xlim([0, 168])
    ax.set_xticks(x_ticks)
    days = ['Sun', 'Mo', 'Tue', 'Wed', 'Thu', 'Fr', 'Sat']
    x_ticklabels = []
    for day in days:
        x_ticklabels.extend(['0', '12\n' + day])
    ax.set_xticklabels(x_ticklabels)

    # Create legend
    legend = []
    for scenario in scenarios_list:
        if scenario.mhp.nominal_flow_rate == 45 and scenario.pv_system.nominal_power == 0 and \
               scenario.battery.max_soc == 0:

            flexibility_cost = scenario.weeklong_hourly_sum_blocked_hours.to_numpy() +\
                               scenario.weeklong_hourly_sum_possible_hours.to_numpy()

            ax.plot(flexibility_cost, drawstyle='steps')
            legend.append('MHP: ' + str(scenario.mhp.nominal_flow_rate) +
                          'l/s - PV: ' + str(scenario.pv_system.nominal_power) + ' kWp - Bat: ' +
                          str(scenario.battery.max_soc) + ' kWh')

        elif scenario.mhp.nominal_flow_rate == 45 and scenario.pv_system.nominal_power == 40 and \
                scenario.battery.max_soc == 40:

            flexibility_cost = scenario.weeklong_hourly_sum_blocked_hours.to_numpy() +\
                               scenario.weeklong_hourly_sum_possible_hours.to_numpy()

            ax.plot(flexibility_cost, drawstyle='steps')
            legend.append('MHP: ' + str(scenario.mhp.nominal_flow_rate) +
                          'l/s - PV: ' + str(scenario.pv_system.nominal_power) + ' kWp - Bat: ' +
                          str(scenario.battery.max_soc) + ' kWh')

        elif scenario.mhp.nominal_flow_rate == 75 and scenario.pv_system.nominal_power == 80 and \
             scenario.battery.max_soc == 80:

            flexibility_cost = scenario.weeklong_hourly_sum_blocked_hours.to_numpy() + \
                               scenario.weeklong_hourly_sum_possible_hours.to_numpy()

            ax.plot(flexibility_cost, drawstyle='steps')
            legend.append('MHP: ' + str(scenario.mhp.nominal_flow_rate) +
                          'l/s - PV: ' + str(scenario.pv_system.nominal_power) + ' kWp - Bat: ' +
                          str(scenario.battery.max_soc) + ' kWh')

    ax.set_ylabel('Required flexibility (combined)')
    ax.set_xlabel("Time [h] and weekdays")

    ax.legend(legend)
    plt.grid()
    plt.show()


def plot_demand_schedules(scenarios_list):
    fig, ax = plt.subplots(ncols=2, nrows=1)
    fig.set_size_inches(w=full_width, h=medium_height)

    legend = []

    scenario1 = scenarios_list[14]
    scenario2 = scenarios_list[7]

    # -- Left -- #
    # Plot total load profile
    ax[0].plot(scenario1.best_dsm_result['total_power_demand'], drawstyle='steps', linestyle='solid')
    legend.append('Total load profile')

    # Plot generation profile
    ax[0].plot(scenario1.power_generation, label='Generation profile',
                     drawstyle='steps', linestyle='solid')
    legend.append('Generation profile')



    ax[0].set_title('MHP ' + str(scenario1.mhp.nominal_flow_rate) + ' l/s - PV: '
                                          + str(scenario1.pv_system.nominal_power) + ' kWp - Bat: '
                                          + str(scenario1.battery.max_soc))

    # Plot full year
    ax[0].set_xticks(list(range(360, 8281, 1440)))
    ax[0].set_xticklabels([calendar.month_abbr[i] for i in range(1, 13, 2)], rotation=70)
    ax[0].set_xlim([0, 8760])

    ax[0].set_xlabel("Time")
    ax[0].set_ylabel("Power [kW]")


    # -- Right --#

    ax[1].plot(scenario2.best_dsm_result['total_power_demand'], drawstyle='steps',
                  linestyle='solid')

    ax[1].plot(scenario2.power_generation, label='Generation profile',
                  drawstyle='steps', linestyle='solid')


    ax[1].set_title('MHP ' + str(scenario2.mhp.nominal_flow_rate) + ' l/s - PV: '
                    + str(scenario2.pv_system.nominal_power) + ' kWp - Bat: '
                    + str(scenario2.battery.max_soc))

    # Plot with selection
    show_start = 24 * 239
    show_end = show_start + 24 * 2

    xticks = list(range(show_start, show_end, 12))


    ax[1].set_xticks(xticks)
    ax[1].set_xticklabels(['0:00', '12:00'] * 2, rotation=70)
    ax[1].set_xlim([show_start, show_end])
    ax[1].set_xlabel("Time")
    ax[1].set_ylabel("Power [kW]")

    fig.legend(legend, loc='upper center', ncol=3)

    plt.show()
    #exit()


def plot_produced_power(scenarios_dict, cache_folder_path):
    fig, ax = plt.subplots(1, 2)

    fig.set_size_inches(w=4.99614, h=2.5)

    # Scenario 1 -> plot 1
    line1 = ax[0].plot(range(0, 8760), scenarios_dict['0']['produced_power'], label='Hydro power generation')

    ax[0].set_ylabel("generated power [kW]")
    ax[0].legend(['Hydro power\n generation'], loc=3)
    ax[0].grid()

    # Set axis ticks and labels
    ax[0].set_xticks(list(range(360, 8281, 1440)))
    ax[0].set_xticklabels([calendar.month_abbr[i] for i in range(1, 13, 2)], rotation=70)
    ax[0].set_ylim(bottom=0)


    # Scenario 2&3 -> plot 2
    show_start = 24*160
    show_end = show_start+24*3

    line3 = ax[1].plot(range(show_start, show_end), scenarios_dict['2']['produced_power'][show_start:show_end],
                       label='Scenario 3', color='C2')
    line2 = ax[1].plot(range(show_start, show_end), scenarios_dict['1']['produced_power'][show_start:show_end],
                       label='Scenario 2', color='C1')
    line1 = ax[1].plot(range(show_start, show_end), scenarios_dict['0']['produced_power'][show_start:show_end],
                       label='Scenario 2', color='C0')

    ax[1].legend(['Scenario 3', 'Scenario 2', 'Scenario 1'])
    ax[1].grid()

    xticks = list(range(show_start, show_end, 12))
    ax[1].set_xticks(xticks)
    ax[1].set_xticklabels(['0:00', '12:00']*3, rotation=70)
    ax[1].set_ylim(bottom=30)
    plt.show()
    exit()


def plot_dsm_logbooks(scenarios_list, cache_folder_path):
    fig, ax = plt.subplots(1, len(scenarios_list))

    #fig.set_size_inches(w=4.99614, h=2.5)
    yticks = range(0, 20000, 5000)

    y_ticklabels = range(0, 3001, 750)
    ylim = [-100, 6000]

    xticks = range(0, 3501, 1166)

    i = 0
    for scenario in scenarios_list:

        gen = scenario.logbook.select("gen")

        # Keys will show in plot as legend
        objectives_to_plot = {
            "Possible hours": scenario.logbook.chapters["possible_hours"].select("min"),
            "Blocked hours": scenario.logbook.chapters["blocked_hours"].select("min"),
            "Power overshoot": scenario.logbook.chapters["power_overshoot"].select("min"),
            "Parallel schedule": scenario.logbook.chapters["parallel_schedule"].select("min")
        }

        lines = []
        legend = []
        for key, values in objectives_to_plot.items():
            lines.append(ax[i].plot(gen, values, label=key))
            legend.append(key)

        ax[i].yaxis.set_ticks(yticks)
        ax[i].set_ylim(ylim)
        ax[i].grid()

        ax[i].xaxis.set_ticks(xticks)

        if i == 0:
            ax[i].set_ylabel("Fitness")
            #ax[i].set_yticklabels(y_ticklabels, rotation=70)

        if i == 1:
            ax[i].set_xlabel("Generation")
            #ax[i].set_yticklabels([])

        if i == 2:
            #ax[i].set_yticklabels([])
            print()

        ax[i].text(.5, .9, 'Scenario '+str(i+1),
                horizontalalignment='center',
                transform=ax[i].transAxes)
        i += 1

    fig.legend(legend, loc='upper center', ncol=2)

    plt.show()
    #exit()
    #plt.savefig(cache_folder_path+'/plots/dsm_fitness_progress.pdf')
    #plt.savefig(cache_folder_path+'/plots/dsm_fitness_progress.pgf')


def plot_dsm_progress_combined(scenarios_list, cache_folder_path):
    fig, ax = plt.subplots()

    fig.set_size_inches(w=full_width, h=small_height)

    i = 0
    for scenario in scenarios_list:

        gen = scenario.logbook.select("gen")

        # Keys will show in plot as legend
        objectives_to_plot = {
            "Fitness": scenario.logbook.chapters["total_fitness"].select("min"),
        }

        lines = []
        legend = []
        for key, values in objectives_to_plot.items():
            lines.append(ax.plot(gen, values))
            #legend.append(key)

    ax.set_xlabel("Number of generations")
    ax.set_ylabel("Fitness of best solution")

    ax.grid()

    plt.show()


def plot_unscheduled_load():
    unscheduled_load = np.array([2, 2, 2, 2, 2.2, 4.9, 5.9, 5.9, 3.8, 3.3, 3.3, 3.3, 3.3, 3.3, 3.3, 3.3, 3.3, 3.8, 5.9, 6.6, 6.2, 5.5, 3.3, 2, 2])
    fig, ax = plt.subplots()

    fig.set_size_inches(full_width, small_height)
    ax.plot(unscheduled_load)

    # Set ticks and ticklabes on x-axis
    x_ticks = list(range(0, 25, 6))
    ax.set_xlim([0, 24])
    ax.set_xticks(x_ticks)
    hours = ['0:00', '6:00', '12:00', '18:00', '24:00']
    ax.set_xticklabels(hours)

    ax.legend(['Daily unscheduled load'])
    ax.set_xlabel("Time [h]")
    ax.set_ylabel("Load [kW]")

    plt.show()


def plot_solar_potential(solar_df):
    date_index = pd.date_range("2019-01-01", periods=8760, freq="H")
    solar_df.set_index(date_index, inplace=True)
    # Correct for system losses
    solar_df['mean'] = solar_df['mean'] * 1.15
    # Monthly energy yield in kWh/kWp
    solar_monthly = solar_df['mean'].groupby(pd.Grouper(freq='M')).sum().to_numpy()/1000

    solar_daily = pd.pivot_table(
        solar_df,
        index=solar_df.index.hour,
        columns=solar_df.index.dayofyear,
        values='mean'  # ,
        # aggfunc='mean'
    )

    solar_daily['mean'] = solar_daily.mean(axis=1)
    solar_daily['std'] = solar_daily.std(axis=1)
    solar_min = solar_daily['mean'].to_numpy() - solar_daily['std']
    solar_max = solar_daily['mean'].to_numpy() + solar_daily['std']

    fig, ax = plt.subplots(nrows=1, ncols=2)
    fig.set_size_inches(full_width, small_height)

    x_ticks_1 = list(range(1,13,1))
    x_ticklabels = [calendar.month_abbr[i] for i in range(1, 13, 1)]

    ax[0].bar(x_ticks_1, solar_monthly)
    ax[0].set_xticks(x_ticks_1)
    ax[0].set_xticklabels(x_ticklabels, rotation=70)

    ax[0].set_ylabel('Solar energy yield [kWh/kWp]')
    ax[0].set_xlabel('Month')

    ax[1].plot(solar_daily['mean'])
    # Set ticks and ticklabes on x-axis
    x_ticks = list(range(0, 25, 6))
    ax[1].set_xlim([0, 24])
    ax[1].set_xticks(x_ticks)
    hours = ['0:00', '6:00', '12:00', '18:00', '24:00']
    ax[1].set_xticklabels(hours)
    ax[1].fill_between(list(range(0,24,1)), solar_min, solar_max,
                     alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848', antialiased=True
                     )
    ax[1].set_ylabel('Solar power [kW/kWp]')
    ax[1].set_xlabel('Time')
    plt.show()

    return


def plot_weekly_operation_hours(scenarios_list):
    fig, axs = plt.subplots(3, 1)

    fig.set_size_inches(full_width, medium_height)

    # Set ticks and ticklabes on x-axis
    x_ticks = list(range(0, 168, 12))

    days = ['Sun', 'Mo', 'Tue', 'Wed', 'Thu', 'Fr', 'Sat']
    x_ticklabels = []
    for day in days:
        x_ticklabels.extend(['0', '12\n' + day])

    for ax in axs:
        ax.set_xlim([0, 168])
        ax.set_xticks(x_ticks)
        ax.set_xticklabels([])
        ax.grid()

    axs[2].set_xticklabels(x_ticklabels)
    axs[2].set_xlabel("Time [h] and weekdays")

    axs[1].set_ylabel('Number of hours of operation')

    consumer_nr = 7
    for scenario in scenarios_list:
        if scenario.mhp.nominal_flow_rate == 45 and scenario.pv_system.nominal_power == 0 and \
                scenario.battery.max_soc == 0:

            consumer_operation = scenario.weeklong_hourly_sum_operation[[consumer_nr]].to_numpy()
            consumer_preferences = scenario.working_hours_df[[consumer_nr]].to_numpy()

            n = 0

            for hours in consumer_operation:
                # pick color
                if consumer_preferences[n] == 3:
                    bar_color = 'indianred'
                elif consumer_preferences[n] == 2:
                    bar_color = 'gold'
                elif consumer_preferences[n] == 1:
                    bar_color = 'seagreen'

                axs[0].bar(n, hours, width=1, color=bar_color)
                axs[0].set_title('MHP: ' + str(scenario.mhp.nominal_flow_rate) +
                          'l/s - PV: ' + str(scenario.pv_system.nominal_power) + ' kWp - Bat: ' +
                          str(scenario.battery.max_soc) + ' kWh')

                n = n + 1

        if scenario.mhp.nominal_flow_rate == 45 and scenario.pv_system.nominal_power == 60 and \
                scenario.battery.max_soc == 60:

            consumer_operation = scenario.weeklong_hourly_sum_operation[[consumer_nr]].to_numpy()
            consumer_preferences = scenario.working_hours_df[[consumer_nr]].to_numpy()

            n = 0

            for hours in consumer_operation:
                # pick color
                if consumer_preferences[n] == 3:
                    bar_color = 'indianred'
                elif consumer_preferences[n] == 2:
                    bar_color = 'gold'
                elif consumer_preferences[n] == 1:
                    bar_color = 'seagreen'

                axs[1].bar(n, hours, width=1, color=bar_color)

                axs[1].set_title('MHP: ' + str(scenario.mhp.nominal_flow_rate) +
                                 'l/s - PV: ' + str(scenario.pv_system.nominal_power) + ' kWp - Bat: ' +
                                 str(scenario.battery.max_soc) + ' kWh')

                n = n + 1

        if scenario.mhp.nominal_flow_rate == 75 and scenario.pv_system.nominal_power == 60 and \
                scenario.battery.max_soc == 60:

            consumer_operation = scenario.weeklong_hourly_sum_operation[[consumer_nr]].to_numpy()
            consumer_preferences = scenario.working_hours_df[[consumer_nr]].to_numpy()

            n = 0

            for hours in consumer_operation:
                # pick color
                if consumer_preferences[n] == 3:
                    bar_color = 'indianred'
                elif consumer_preferences[n] == 2:
                    bar_color = 'gold'
                elif consumer_preferences[n] == 1:
                    bar_color = 'seagreen'

                axs[2].bar(n, hours, width=1, color=bar_color)

                axs[2].set_title('MHP: ' + str(scenario.mhp.nominal_flow_rate) +
                                 'l/s - PV: ' + str(scenario.pv_system.nominal_power) + ' kWp - Bat: ' +
                                 str(scenario.battery.max_soc) + ' kWh')

                n = n + 1

    # Add legend
    legend = [Line2D([0], [0], color='seagreen', lw=4, label='Preferred hours'),
              Line2D([0], [0], color='gold', lw=4, label='Unfavored hours'),
              Line2D([0], [0], color='indianred', lw=4, label='Highly unfavored hours')]

    # Put a legend below current axis
    ax.legend(handles=legend, loc='upper center', bbox_to_anchor=(0.5, 4.3),
              fancybox=True, ncol=3)

    plt.subplots_adjust(hspace=0.333, top=0.874, bottom=0.13, left=0.095, right=0.976)
    plt.tight_layout()
    plt.show()