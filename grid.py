import UI_user_input as ui
import econ_functions as ef

class Grid:
    def __init__(self):

        self.capex = None
        self.opex = None
        self.annual_cost = None

        self.calculate_cost_data()  # Calculate cost data on init

    def calculate_cost_data(self):
        self.capex = ui.grid_capex
        self.opex = ui.grid_opex

        crf = ef.crf(ui.discount_rate, ui.grid_lifetime)
        self.annual_cost = ef.annual_cost(crf, self.capex, self.opex)