import pandas as pd
import numpy as np

import final_eval
import UI_deap_dsm_config


class Scenario:
    def __init__(self, scenario_id, mhp, pv_system, battery, grid, unscheduled_load_curve):
        self.scenario_id = scenario_id

        # Initiate system components
        self.battery = battery
        self.mhp = mhp
        self.pv_system = pv_system
        self.grid = grid

        self.power_generation = self.calculate_power_generation(mhp.hydro_power_output, pv_system.solar_power_output)
        self.power_availability = self.calculate_power_availability(unscheduled_load_curve)

        self.annual_energy_generated = self.power_generation.sum()
        # Dict for statistics calculated from results of this scenario after DSM
        self.power_stats = {}

        # Variable to hold hof_evolution (=GA result)
        self.hof_evolution = None
        # Variable to hold logbook of GA run
        self.logbook = None

        # Calculate scenario cost data
        self.capex = None
        self.opex = None
        self.annual_cost = None
        self.calculate_scenario_cost()  # Run scenario cost calculation on init

        self.best_dsm_result = None
        self.annual_energy_delivered = None  # in kWh, calculated after DSM based on results
        self.lcoe = None

        self.annual_demand_curve = None
        self.weeklong_hourly_mean_demand = None
        self.weekly_demand_curves = None

        self.annual_blocked_hours_curve = None
        self.annual_possible_hours_curve = None

        self.weeklong_hourly_mean_blocked_hours = None
        self.weeklong_hourly_mean_possible_hours = None

        self.weeklong_hourly_sum_blocked_hours = None
        self.weeklong_hourly_sum_possible_hours = None

        self.weeklong_hourly_sum_operation = None

        self.consumer_operation_schedule = None

    def calculate_power_generation(self, mhp_generation, pv_generation):
        return mhp_generation + pv_generation

    def calculate_power_availability(self, unscheduled_load_curve):
        # Subtract unscheduled load curve from produced power and return as
        power_availability = np.array(self.power_generation) - np.array(unscheduled_load_curve) - \
                             UI_deap_dsm_config.power_buffer
        return np.rint(power_availability)

    # ---- DSM result analysis ----
    def get_best_dsm_result(self, consumers_number, job_dict, working_hours_array):
        # For DSM eval use results without power buffer -> power availability + buffer

        self.best_dsm_result = final_eval.final_eval_fitness(individual=self.hof_evolution[-1][0],
                                                             optimization_length=UI_deap_dsm_config.optimization_length,
                                                             optimization_start=UI_deap_dsm_config.optimization_start,
                                                             consumers_number=consumers_number,
                                                             job_dict=job_dict,
                                                             power=self.power_availability + UI_deap_dsm_config.power_buffer,
                                                             working_hours=working_hours_array,
                                                             battery=self.battery,
                                                             )

    def calculate_annual_energy_delivered(self, unscheduled_load_curve):
        # Calculate total energy delivered: Unscheduled load + scheduled load from dsm - not delivered power (overshoot)
        self.annual_energy_delivered = np.array(unscheduled_load_curve).sum() \
                                       + self.best_dsm_result['total_power_demand'].sum() \
                                       - self.best_dsm_result['power_overshoot']

    def analyse_demand_curve(self, unscheduled_load_curve):

        if unscheduled_load_curve != 0:  # otherwise benchmark
            self.annual_demand_curve = pd.DataFrame(np.array(unscheduled_load_curve)
                                                    + self.best_dsm_result['total_power_demand'], columns=['power_demand'])
            date_index = pd.date_range("2021-01-01", periods=8760, freq="H")

        date_index = pd.date_range("2019-01-01", periods=8760, freq="H")
        self.annual_demand_curve.set_index(date_index, inplace=True)

        self.weeklong_hourly_mean_demand = self.annual_demand_curve.groupby(
            self.annual_demand_curve.index.dayofweek * 24 + self.annual_demand_curve.index.hour
        ).mean().rename_axis('HourOfWeek')

        self.weekly_demand_curves = pd.pivot_table(
            self.annual_demand_curve,
            index=self.annual_demand_curve.index.dayofweek,
            columns=self.annual_demand_curve.index.week,
            values='power_demand'  # ,
            # aggfunc='mean'
        )

    def analyse_flexibility_schedule(self):
        self.annual_blocked_hours_curve = pd.DataFrame(self.best_dsm_result['consumers_blocked_hours_schedule'])
        self.annual_possible_hours_curve = pd.DataFrame(self.best_dsm_result['consumers_possible_hours_schedule'])

        date_index = pd.date_range("2019-01-01", periods=8760, freq="H")
        self.annual_possible_hours_curve.set_index(date_index, inplace=True)
        self.annual_blocked_hours_curve.set_index(date_index, inplace=True)

        self.weeklong_hourly_mean_blocked_hours = self.annual_blocked_hours_curve.groupby(
            self.annual_blocked_hours_curve.index.dayofweek * 24 + self.annual_blocked_hours_curve.index.hour
        ).mean().rename_axis('HourOfWeek')

        self.weeklong_hourly_mean_possible_hours = self.annual_possible_hours_curve.groupby(
            self.annual_possible_hours_curve.index.dayofweek * 24 + self.annual_possible_hours_curve.index.hour
        ).mean().rename_axis('HourOfWeek')

        self.weeklong_hourly_sum_blocked_hours = self.annual_blocked_hours_curve.groupby(
            self.annual_blocked_hours_curve.index.dayofweek * 24 + self.annual_blocked_hours_curve.index.hour
        ).sum().rename_axis('HourOfWeek')

        self.weeklong_hourly_sum_possible_hours = self.annual_possible_hours_curve.groupby(
            self.annual_possible_hours_curve.index.dayofweek * 24 + self.annual_possible_hours_curve.index.hour
        ).sum().rename_axis('HourOfWeek')

    def calculate_consumer_operation_stats(self, working_hours_array):
        # Consumer operation stats
        self.consumer_operation_schedule = self.best_dsm_result['operation_schedule']
        # Replace no operation None with 0
        np.place(self.consumer_operation_schedule, self.consumer_operation_schedule == None, 0)

        # Replace operation job ID with 1
        self.consumer_operation_schedule = np.clip(self.consumer_operation_schedule, a_min=None, a_max=1)

        # Turn into pandas df
        self.consumer_operation_schedule = pd.DataFrame(self.consumer_operation_schedule)

        # Transpose df
        self.consumer_operation_schedule = self.consumer_operation_schedule.transpose()

        # Give datetime index
        date_index = pd.date_range("2019-01-01", periods=8760, freq="H")
        self.consumer_operation_schedule.set_index(date_index, inplace=True)

        # Sum over weeks
        self.weeklong_hourly_sum_operation = self.consumer_operation_schedule.groupby(
            self.consumer_operation_schedule.index.dayofweek * 24 + self.consumer_operation_schedule.index.hour
        ).sum().rename_axis('HoursOfWeek')

        # --- For coloring of operation hours analysis plot ---
        # get average preferred weekly working hours
        working_hours_df = pd.DataFrame(working_hours_array)
        working_hours_df = working_hours_df.transpose()
        # Give datetime index
        date_index = pd.date_range("2019-01-01", periods=8760, freq="H")
        working_hours_df.set_index(date_index, inplace=True)

        # Mean over weeks
        working_hours_df = working_hours_df.groupby(
            working_hours_df.index.dayofweek * 24 + working_hours_df.index.hour
        ).mean().rename_axis('HoursOfWeek')

        # Round to int
        self.working_hours_df = working_hours_df.round(decimals=0)

    # ---- Econ calculations ----
    def calculate_scenario_cost(self):
        self.capex = self.battery.capex + self.mhp.capex + self.pv_system.capex + self.grid.capex
        self.opex = self.battery.opex + self.mhp.opex + self.pv_system.opex + self.grid.opex
        self.annual_cost = self.battery.annual_cost + self.mhp.annual_cost + self.pv_system.annual_cost + self.grid.annual_cost

    def calculate_lcoe(self):
        self.lcoe = self.annual_cost/self.annual_energy_delivered