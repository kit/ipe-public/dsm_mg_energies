import time

from deap_run import deap_run


def scenario_worker_function(scenarios_return_list, scenario, optimization_start, optimization_length, consumers_number, job_dict, power_availability, working_hours_array):

    print('Scenario ' + str(scenario.scenario_id) + ' started')
    t_start = time.perf_counter()
    population, hof_evolution, logbook = deap_run(
        optimization_start=optimization_start,
        optimization_length=optimization_length,
        consumers_number=consumers_number,
        job_dict=job_dict,
        power_availability=power_availability,
        working_hours_array=working_hours_array)

    scenario.hof_evolution = hof_evolution
    scenario.logbook = logbook

    scenarios_return_list.append(scenario)

    print('Scenario ' + str(scenario.scenario_id) + ' finished: ' + str(time.perf_counter()-t_start))
