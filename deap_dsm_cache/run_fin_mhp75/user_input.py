import numpy as np
# ---- User input ----

# -- Economic data
discount_rate = np.array([0.1])
capex_variation = 1

# -- MHP
mhp_steps = [75]

mhp_efficiency = 0.75
mhp_head = 120

mhp_capex_per_kw = 4000 * capex_variation
mhp_opex_per_kw = 100
mhp_lifetime = 40  # years

# -- PV system
pv_steps = [0, 20, 40, 60, 80]

pv_capex_per_kwp = 1600 * capex_variation
pv_opex_per_kwp = 15
pv_lifetime = 20  # years

# -- Battery
battery_steps = [0, 20, 40, 60, 80]
battery_c_rate = 1
battery_round_trip_efficiency = 0.9
battery_dod = 0.8

battery_capex_per_kwh = 900*capex_variation
battery_opex_per_kwh = 5
battery_lifetime = 10  # years

# -- Grid fixed cost
grid_capex = 110000
grid_lifetime = 20
grid_opex = 0.02 * grid_capex


