# ---- GA parameter ----
pop_size = 600
n_gen = 3500

tourn_size = 3

crossover_prob = 0.5
mut_prob = 1

# Objective weights -> all minimized
power_objective = 3
blocked_hours_objective = 2
possible_hours_objective = 1
parallel_schedule_objective = 100

# Buffer in kW of that scheduled consumption needs to be below produced power [kW].
power_buffer = 1


# --- General parameters ---
# Duration of DSM timeframe
optimization_start = 0
optimization_length = 8760



