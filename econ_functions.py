def crf(discount_rate, lifetime):
    return (discount_rate*(1+discount_rate)**lifetime)/((1+discount_rate)**lifetime - 1)


def annual_cost(crf, capex, opex):
    return capex * crf + opex

