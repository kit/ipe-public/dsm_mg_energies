import numpy as np

import UI_user_input as ui
import econ_functions as ef


class Mhp:
    def __init__(self, nominal_flow_rate, flow_rate_data):
        self.nominal_flow_rate = nominal_flow_rate
        self.nominal_power = np.round(nominal_flow_rate * ui.mhp_head * ui.mhp_efficiency * 9.81 * (1/1000))

        self.head = ui.mhp_head
        self.efficiency = ui.mhp_efficiency

        self.hydro_power_output = self.calculate_power_output(flow_rate_data)

        self.capex = None
        self.opex = None
        self.annual_cost = None

        self.calculate_cost_data()  # Calculate cost data on init

    def calculate_power_output(self, flow_rate):
        # round to kW and clip values larger nominal power
        return np.round(
            np.clip(flow_rate * self.efficiency * 9.81 * self.head * (1/1000), a_min=0, a_max=self.nominal_power))

    def calculate_cost_data(self):
        self.capex = ui.mhp_capex_per_kw * self.nominal_power
        self.opex = ui.mhp_opex_per_kw * self.nominal_power

        crf = ef.crf(ui.discount_rate, ui.mhp_lifetime)
        self.annual_cost = ef.annual_cost(crf, self.capex, self.opex)

