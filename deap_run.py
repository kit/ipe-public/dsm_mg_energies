from deap import base
from deap import tools

from scoop import futures

import random

import UI_deap_dsm_config
import deap_dsm
import custom_stats

# Creator helper function (see https://github.com/DEAP/deap/issues/57)
creator = None


def set_creator(cr):
    global creator
    creator = cr


def deap_run(optimization_start, optimization_length, consumers_number, job_dict, power_availability,
             working_hours_array, cache_folder_path, scenario_id, battery):

    pop_size = UI_deap_dsm_config.pop_size
    n_gen = UI_deap_dsm_config.n_gen
    tourn_size = UI_deap_dsm_config.tourn_size

    crossover_prob = UI_deap_dsm_config.crossover_prob
    mut_prob = UI_deap_dsm_config.mut_prob

    toolbox = base.Toolbox()

    # Alternative random seed
    toolbox.register("random_seed", deap_dsm.init_random_seed, job_dict)

    # Random seed individual
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.random_seed)

    # Register population
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    toolbox.register("evaluate", deap_dsm.eval_fitness,
                     optimization_length=optimization_length,
                     optimization_start=optimization_start,
                     consumers_number=consumers_number,
                     job_dict=job_dict,
                     power=power_availability,
                     working_hours=working_hours_array,
                     battery=battery
                     )

    toolbox.register("two_point_crossover", tools.cxTwoPoint)
    toolbox.register("mutate", deap_dsm.dsm_mutate, job_dict=job_dict)

    toolbox.register("select_tourn", tools.selTournament, tournsize=tourn_size)

    # Register scoops parallel mapping for DEAP
    toolbox.register("map", futures.map)

    random.seed(64)

    pop = toolbox.population(n=pop_size)
    hof = tools.HallOfFame(1)

    stats = custom_stats.customStats(lambda ind: ind.fitness.values, ['total_fitness', 'possible_hours',
                                                                      'blocked_hours', 'power_overshoot',
                                                                      'parallel_schedule'])

    pop, logbook, hof_evolution = deap_dsm.main_ga(pop, toolbox, cxpb=crossover_prob, mutpb=mut_prob, ngen=n_gen,
                                                   stats=stats, halloffame=hof, job_dict=job_dict,
                                                   cache_folder_path=cache_folder_path, scenario_id=scenario_id)

    print('GA Result: \n')

    print(logbook)

    return pop, hof_evolution, logbook
