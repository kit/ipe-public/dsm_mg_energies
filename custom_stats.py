import numpy as np


class customStats:
    """
    Similar to DEAP's default Stats class. Adapted to log stats for multiple objectives
    """
    def __init__(self, key, stat_values):
        self.stat_values = stat_values
        self.key = key
        self.stat_values = stat_values

    def compile(self, population):
        entry = {}

        fitness_values = tuple(self.key(elem) for elem in population)

        avg_value = np.mean(fitness_values, axis=0)
        std_value = np.std(fitness_values, axis=0)
        min_value = np.min(fitness_values, axis=0)
        max_value = np.max(fitness_values, axis=0)

        i = 0
        for value in self.stat_values:
            entry[value] = {
                'avg': avg_value[i],
                'std': std_value[i],
                'min': min_value[i],
                'max': max_value[i]
            }

            i = i + 1

        return entry