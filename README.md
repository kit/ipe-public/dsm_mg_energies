# Implementation of GA presented in "Dimensioning of Microgrid Capacities for Productive Use of Energy considering Demand Side Flexibilities" (Kraft, Luh)


## Instructions to run:

- install Python 3.8 in a conda environment (e.g. using miniconda: https://docs.conda.io/en/latest/miniconda.html)
- run pip install -r requirements.txt to install required packages

- run GA with data of the presented test case:
    - to use Scoop multiprocessing the scrip needs to be called through the terminal with ``python -m scoop main.py``
    - on prompt: give run a name. Results will be stored in folder containing the specified name.
- read results of cached runs
    - on prompt: type "read"
    - select folder of run to analyse
        - 3 folders for the results of the test case already exist: Two for each MHP dimensioning (45 and 75 l/s nominal flow) and one with both combined