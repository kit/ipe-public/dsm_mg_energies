import numpy as np
import time
import UI_deap_dsm_config


def final_eval_fitness(individual, optimization_length, optimization_start, consumers_number, job_dict, power, working_hours,
                 battery):
    """
    Fitness function of this DEAP GA

    :param individual:
    :param optimization_length: number of timesteps in which jobs can be scheduled
    :param optimization_start: starting timestep relative to start of year
    :param consumers_number: number of different consumers
    :param job_dict: dict containing job information
    :param power: available power
    :param working_hours: available working_hours
    :param battery: battery object
    :param verbose: default False, print eval results if True
    :param final_eval:
    :return: tuple with optimization objectives
    """

    eval_start = time.perf_counter()

    # Objective variables
    power_overshoot = 0  # kWh of demand overshooting availability
    possible_hours = 0  # Number of hours of operation during possible hours
    blocked_hours = 0  # Number of hours of operation during blocked hours
    parallel_schedule_penalty = 0  # Number of hours of double operation of one consumer -> not to be permitted!

    job_id = 0

    # Initialize power and operation schedule
    power_demand = np.zeros(24 * 365)
    operation_schedule = np.full((consumers_number, 24 * 365), None)

    consumers_blocked_hours_schedule = np.zeros(24 * 365, None)  # first row
    consumers_possible_hours_schedule = np.zeros(24 * 365, None)  # first row


    # Reset this individuals job_fitness list. One entry for each job reset to None.
    total_job_fitness = np.zeros(len(individual))

    # ---- Loop through all jobs in this individual
    while job_id < len(individual):

        # Individual job_fitness variable
        job_fitness = 0

        # Get job details
        start_time = individual[job_id]  # Start time is int value from individual list.

        job = job_dict[job_id]  # get this job's parameters

        i = start_time
        # Loop through every of this jobs timesteps.
        while i < start_time + job['duration'] and i < optimization_start + optimization_length:

            # ---- Power demand ----
            # Add this jobs demand to total demand

            power_demand[i] = power_demand[i] + job['demand']

            # ---- Consumer operation ----
            # Set operation for this consumer in the operation schedule
            # ~ 0.15s per individual (full problem) (~35%)

            # Check if consumer has already scheduled operation in this timeframe

            if operation_schedule[job['consumer_id']][i] is None:  # If no operation yet

                # Set operation.
                # !! CHANGE: Operation marked with job ID -> basis to identify jobs that contribute to power overshoot
                operation_schedule[job['consumer_id']][i] = job_id

                # Check if in blocked hours
                if working_hours[job['consumer_id']][i] == 3:  # If blocked
                    job_fitness = job_fitness + 2  # Update job fitness
                    blocked_hours = blocked_hours + 1  # Update blocked_hours counter
                    consumers_blocked_hours_schedule[i] += 1  # Add one blocked hour at this timestep

                elif working_hours[job['consumer_id']][i] == 2:  # If possible
                    job_fitness = job_fitness + 1  # Update job_fitness
                    possible_hours = possible_hours + 1  # Update possible_hours counter
                    consumers_possible_hours_schedule[i] += 1  # Add one possible hour at this timestep

            else:  # If scheduled parallel, add penalty
                parallel_schedule_penalty = parallel_schedule_penalty + 1

                # Add job_fitness penalty
                job_fitness = job_fitness + 1

            i = i + 1

        # Save job_fitness of this job.
        total_job_fitness[job_id] = job_fitness

        job_id = job_id + 1


    # ---- End job loop
    # ----- Power demand evaluation
    power_eval_start = time.perf_counter()

    # Calculate difference of produced power and demand
    power_diff = power - power_demand

    # Calculate power overshoot
    # power_overshoot = ((power_diff < 0) * power_diff).sum(axis=0) * (-1) -> currently done in iteration

    t = 0
    power_overshoot = 0
    for power_value in power_diff:  # Loop through power_diff array (every timestep of year)

        if battery.max_soc > 0:  # Check if battery exists
            # Battery model
            if power_value > 0:  # excess power -> charge battery
                charged_power = battery.charge_battery(power_value, t)  # charge battery and get actually charged power
                power_value = power_value - charged_power  # subtract power delivered to battery from power value

            elif power_value <= 0:  # lack of power -> discharge battery (or 0 -> to fill SOC list)
                discharged_power = battery.discharge_battery(power_value * -1, t)  # discharge battery (positive power)
                power_value = power_value + discharged_power  # Add power discharged from battery to power value
            # --- end battery model

        # Update job_fitness with respect to power overshoot
        if power_value < 0:  # Check if negative -> = power_overshoot
            power_overshoot = power_overshoot - power_value  # add power overshoot
            # Select this timestep in the operation schedule
            operation = operation_schedule[:, t]
            # Loop through all scheduled jobs at this timestep -> get their ID
            for operation_job_id in operation:
                if operation_job_id is not None:  # If 0 -> no operation
                    # Add penalty to the job_fitness of the jobs at this timestep
                    # (inverted, job_fitness is best when 0 and worse when larger)
                    total_job_fitness[operation_job_id] += (-power_value)
        t += 1

    # Normalize job_fitness
    # Normalized to value between 1 (=highest job_fitness penalty) and 0 (=lowest job_fitness penalty)
    # This is later used as probability of mutation of every single job
    total_job_fitness = total_job_fitness / np.max(total_job_fitness)

    # print('Power_eval time: '+str(time.perf_counter()-power_eval_start))
    # ----- End power eval

    # Calculate total fitness
    total_fitness = power_overshoot * UI_deap_dsm_config.power_objective + \
                    possible_hours * UI_deap_dsm_config.possible_hours_objective + \
                    blocked_hours * UI_deap_dsm_config.blocked_hours_objective + \
                    parallel_schedule_penalty * UI_deap_dsm_config.parallel_schedule_objective

    dsm_result_dict = {
        "power_overshoot": power_overshoot,  # score
        "possible_hours": possible_hours,  # score
        "blocked_hours": blocked_hours,  # score
        "parallel_schedule_penalty": parallel_schedule_penalty,  # score
        "total_fitness": total_fitness,  # score
        # 2D array with every consumer's power demand for every timestep
        "total_power_demand": power_demand,  # array with total power demand for every timestep
        "operation_schedule": operation_schedule,  # working_hours usage matrix
        "consumers_possible_hours_schedule": consumers_possible_hours_schedule,
        "consumers_blocked_hours_schedule": consumers_blocked_hours_schedule
    }

    return dsm_result_dict

    # print('Total eval: '+str(time.perf_counter()-eval_start))

