import numpy as np
import matplotlib.pyplot as plt

import UI_user_input as ui
import econ_functions as ef


class Battery:
    def __init__(self, capacity):
        self.max_soc = capacity
        self.min_soc = round(capacity * (1-ui.battery_dod))
        self.max_power = capacity * ui.battery_c_rate
        self.discharge_eff = round(ui.battery_round_trip_efficiency ** 0.5, 2)  # round trip efficiency evenly spread on charge and discharge
        self.charge_eff = round(ui.battery_round_trip_efficiency ** 0.5, 2)  # round trip efficiency evenly spread on charge and discharge

        self.soc = self.max_soc  # initially fully charged
        self.soc_list = np.zeros(24*365)  # list for SOC state throughout year

        self.capex = None
        self.opex = None
        self.annual_cost = None

        self.calculate_cost_data()  # Calculate cost data on init

    def charge_battery(self, available_power, timestep):
        soc_diff = self.max_soc - self.soc  # calculate available capacity for charging
        if soc_diff > 0:  # capacity available to charge
            # Charge battery
            if soc_diff < self.max_power:  # remaining capacity limits charging power
                charge = soc_diff * (1/self.charge_eff)  # required charge before losses to fill battery after losses
            else:
                charge = self.max_power  # battery can be charged with maximum power
        else:  # no capacity to charge
            charge = 0

        # charge is limited by available power
        if charge > available_power:
            charge = available_power

        # Apply losses
        charge_after_losses = charge * self.charge_eff

        # Update SOC with charge after losses!
        self.soc = self.soc + charge_after_losses
        # Update SOC list with SOC state of this timestep
        self.soc_list[timestep] = self.soc

        return charge  # returns power actually given to battery (before losses)

    def discharge_battery(self, demanded_power, timestep):
        soc_diff = self.soc - self.min_soc  # calculate available capacity for discharging
        if soc_diff > 0:  # capacity to available discharge
            # Discharge battery
            if soc_diff < self.max_power:
                # remaining capacity limits discharging power
                discharge = soc_diff
            else:
                discharge = self.max_power
        else:  # no capacity to charge
            discharge = 0

        # Apply losses
        discharge_after_losses = discharge * self.discharge_eff

        # Discharge is limited by demanded power
        if discharge_after_losses > demanded_power:  # Discharge after losses is greater than demanded power
            discharge_after_losses = demanded_power  # Limit to demanded power
            discharge = discharge_after_losses * (1/self.discharge_eff)  # Adapt discharge before losses

        # Update SOC with discharge before losses!
        self.soc = self.soc - discharge

        # Update SOC list with SOC state of this timestep
        self.soc_list[timestep] = self.soc

        return discharge_after_losses  # returns power actually returned from battery (after losses)

    def plot_soc_curve(self):

        fig, ax = plt.subplots()
        ax.plot(self.soc_list)
        plt.show()

    def plot_battery_power(self):
        fig, ax = plt.subplots()
        ax.plot(self.battery_power, drawstyle='steps')
        plt.show()

    def calculate_cost_data(self):
        self.capex = ui.battery_capex_per_kwh*self.max_soc
        self.opex = ui.battery_opex_per_kwh*self.max_soc

        crf = ef.crf(ui.discount_rate, ui.battery_lifetime)
        self.annual_cost = ef.annual_cost(crf, self.capex, self.opex)

    def analyse_operation(self):
        self.battery_power = np.zeros(24*365)
        n = 0
        for soc in np.nditer(self.soc_list):
            if n == 0:  # Battery is fully charged on first timestep
                self.battery_power[n] = soc - self.max_soc
            else:
                # Power is current timestep's soc minus previous timestep's soc
                # -> discharging: negative power, charging: positive power
                self.battery_power[n] = soc - self.soc_list[n-1]
            n = n + 1

        self.battery_annual_cycles = (np.clip(self.battery_power, a_min=None, a_max=0).sum() * -1) / self.max_soc



