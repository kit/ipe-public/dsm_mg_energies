import numpy as np
import copy

from deap import base
from deap import tools

import UI_deap_dsm_config


def create_consumption_dict(consumer_list_df, consumers_list, scheduling_start, scheduling_end):

    dsm_prep_obj = DeapDSMPrep(None, consumer_list_df, consumers_list, scheduling_start, scheduling_end)

    # Create job_dict
    dsm_prep_obj.create_job_dict(consumers_list)
    job_dict = dsm_prep_obj.job_dict

    # Get unscheduled load_curve as list (rounded to 2 decimals)
    unscheduled_load_curve = np.around(dsm_prep_obj.calculate_unscheduled_load_curve(), decimals=2).tolist()

    consumption_dict = {
        'job_dict': job_dict,
        'unscheduled_load_curve': unscheduled_load_curve
    }

    return consumption_dict


def create_working_hours_array(consumers_list):

    working_hours_array = [None] * len(consumers_list)
    for consumer in consumers_list:
        # Add this consumers operation times to dict with consumer_id as key
        working_hours_array[int(consumer.consumer_id)] = consumer.operation_times.tolist()

    return working_hours_array


class DeapDSMPrep:
    toolbox: base.Toolbox
    logbook: tools.Logbook

    def __init__(self, scenario, consumer_list_df, consumers_list, scheduling_start, scheduling_end):
        self.scenario = scenario
        self.consumer_list_df = consumer_list_df

        self.power_availability = None
        self.job_dict = {}

        self.jobs_number = 0  # Variable to hold total number of jobs
        self.total_energy_consumption = 0  # Calculated in job dict creation
        self.total_energy_produced = 0  # Calculate in resource creation

        self.optimization_start = scheduling_start  # First timestep of optimization period.
        self.optimization_length = scheduling_end - scheduling_start  # Number of timesteps of optimization period.
        self.consumers_number = len(consumers_list)
        self.consumers_list = consumers_list

        # List containing best individuals of each generation
        self.best_individuals = []

    def calculate_unscheduled_load_curve(self):
        # Calculate hourly unscheduled load curve for one year and return as numpy array
        # TO DO: Adapt to flexible lengths
        unscheduled_load_curve = np.array(list(self.consumer_list_df[range(0, 24, 1)].sum()) * 365)

        return unscheduled_load_curve

    def calculate_power_availability(self):
        # Subtract unscheduled load curve from produced power and return as
        self.power_availability = np.array(self.scenario.production_curve_dict['mean']['P'] \
                             - self.calculate_unscheduled_load_curve())

        self.total_energy_produced = self.power_availability.sum()

    def create_job_dict(self, consumers_list):
        # Create dict containing every jobs information in the following format:
        # { job_id:
        #   { consumer_id: [int],
        #   consumer_name: [str]
        # 	duration: hours [int]
        # 	demand: P in kW [int]
        # 	release: hours [int]
        # 	deadline: hours [int]
        # 	delay: hours [int] }
        #   parent_job: [int]  # First job of this consumer
        # }

        # Increasing job id for all consumers
        job_id = 0

        for consumer in consumers_list:
            # Get this consumer's jobs as dataframe
            consumer_jobs = consumer.operation_blocks

            # Get this consumer's number of jobs
            consumer_jobs_number = len(consumer_jobs.index)

            # Get deap job ID of this consumer's first job
            first_job_id = job_id

            # Add column to hold deap job id
            consumer_jobs['deap_job_id'] = ""

            for index, job in consumer_jobs.iterrows():
                # Copy this jobs deap_job_id (ID in deap individual) in consumer_jobs df
                consumer_jobs.at[index, 'deap_job_id'] = copy.copy(job_id)

                # Turn release and deadline into hours (int)
                if job['Release'] == 'none':
                    release = self.optimization_start
                else:
                    release = job['Release'] * 24

                if job['Deadline'] == 'none':
                    # Deadline is last day of observed timeframe
                    deadline = self.optimization_length + self.optimization_start - 1
                else:
                    deadline = job['Deadline'] * 24

                self.job_dict[job_id] = {
                    'consumer_id': consumer.consumer_id,
                    'consumer_name': consumer.consumer_name,
                    'duration': int(job['Duration']),
                    'demand': job['Power'],
                    'release': release,  # First allowed timestep of this job
                    'deadline': deadline  # Last allowed timestep of this job.
                }

                self.total_energy_consumption = self.total_energy_consumption + job['Duration']

                job_id = job_id + 1  # Increment job_id

        self.jobs_number = len(self.job_dict)

