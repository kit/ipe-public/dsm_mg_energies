import numpy as np
import matplotlib.pyplot as plt
import UI_user_input as ui
import econ_functions as ef

class PvSystem:
    def __init__(self, nominal_power, solar_potential):
        # Cost data
        self.nominal_power = nominal_power  # [kWp]
        self.solar_power_output = self.calculate_power_output(solar_potential)

        self.capex = None
        self.opex = None
        self.annual_cost = None

        self.calculate_cost_data()  # Calculate cost data on init

    def calculate_power_output(self, solar_potential):
        return np.round(solar_potential * (1/1000) * self.nominal_power)  # round to kW

    def calculate_cost_data(self):
        self.capex = ui.pv_capex_per_kwp * self.nominal_power
        self.opex = ui.pv_opex_per_kwp * self.nominal_power

        crf = ef.crf(ui.discount_rate, ui.pv_lifetime)
        self.annual_cost = ef.annual_cost(crf, self.capex, self.opex)
