import datetime
import time

import pickle
import os
import shutil

import pandas as pd

import tkinter as tk
from tkinter import filedialog

import scenario as c_s
import consumers as c_c

import deap_dsm_prep as c_ddp

from deap import creator
from deap import base

from deap_run import set_creator
from deap_run import deap_run

from battery import Battery
from pv_system import PvSystem
from mhp import Mhp
from grid import Grid

import UI_user_input

import plotting


# Creator functions in main to make SCOOP work
set_creator(creator)
# Register fitness
creator.create("FitnessMin", base.Fitness, weights=(-1.0, -1.0, -1.0, -1.0, -1.0))
# Create individual
# job_fitness attribute for list to save fitness rating for each individual job's fitness rating
creator.create("Individual", list, typecode='i', fitness=creator.FitnessMin, job_fitness=[])


if __name__ == '__main__':

    t_total_start = time.perf_counter()

    run_name = input("Run name ('read' to get cached run): ")

    # Read solar potential data
    with open('./data/solar_potential.csv') as solar_potential_csv:
        solar_potential_df = pd.read_csv(solar_potential_csv)
        solar_potential = solar_potential_df['mean'].to_numpy()

    # Read flow rate data
    with open('./data/flow_rate.csv') as flow_rate_csv:
        flow_rate_df = pd.read_csv(flow_rate_csv)
        flow_rate = flow_rate_df['flow_rate'].to_numpy()

    # ----- Consumers -----

    # Create instance of ConsumersData with specified consumer data input file
    consumers_data = c_c.ConsumerData(UI_user_input.consumer_data_path)

    # Create Consumer instance for every scheduled consumer
    scheduled_consumers_list = []  # List to hold consumer instances.
    consumer_id = 0  # Consumer IDs starting at 0
    for consumer_name, scheduled_consumer_data in consumers_data.scheduled_consumers_data.items():
        consumer = c_c.Consumer(consumer_id, consumer_name, scheduled_consumer_data)
        consumer.define_operation_times()
        consumer.define_operation_blocks()

        scheduled_consumers_list.append(consumer)  # Add this consumer to scheduled consumers list
        consumer_id = consumer_id + 1  # Increment consumer_id

    # Create dict containing all jobs of scheduled consumers for efficient access by scheduling GA
    consumption_dict = c_ddp.create_consumption_dict(consumers_data.consumers_list_df, scheduled_consumers_list,
                                                     scheduling_start=consumers_data.scheduling_start,
                                                     scheduling_end=consumers_data.scheduling_end)

    # Create array containing every consumer's working hours
    working_hours_array = c_ddp.create_working_hours_array(consumers_list=scheduled_consumers_list)

    if run_name == 'read':  # Read cached files from previous run
        root = tk.Tk()
        root.withdraw()
        cache_folder_path = filedialog.askdirectory(initialdir='./deap_dsm_cache/')

        with open(cache_folder_path+'/scenarios_list.pkl', 'rb') as cache_file:
            cached_scenarios_list = pickle.load(cache_file)

        scenario_id = 0
        for scenario in cached_scenarios_list:

            # ---- Analyse results ----
            scenario.get_best_dsm_result(consumers_number=len(scheduled_consumers_list),
                                         job_dict=consumption_dict['job_dict'],
                                         working_hours_array=working_hours_array
                                         )
            scenario.analyse_flexibility_schedule()

            scenario.calculate_consumer_operation_stats(working_hours_array=working_hours_array)

            scenario.battery.analyse_operation()
            scenario.calculate_annual_energy_delivered(consumption_dict['unscheduled_load_curve'])
            scenario.calculate_lcoe()
            scenario.analyse_demand_curve(consumption_dict['unscheduled_load_curve'])

            print('\nScenario: ' + str(scenario.scenario_id))
            print('LCOE: ' + str(scenario.lcoe))
            print('Capex: ' + str(scenario.capex))
            print('Opex: ' + str(scenario.opex))
            annual_energy_generated = scenario.pv_system.solar_power_output.sum() + scenario.mhp.hydro_power_output.sum()
            print('Energy generated: ' + str(annual_energy_generated))
            print('Load factor: ' + str(scenario.annual_energy_delivered / annual_energy_generated))

        # ----- Plotting ----- #

        plotting.plot_demand_schedules(cached_scenarios_list)

        plotting.plot_dsm_progress_combined(cached_scenarios_list, cache_folder_path)

        plotting.plot_weekly_flexibility_demand(cached_scenarios_list)

        plotting.plot_lcoe_flexibility_bar_chart(cached_scenarios_list)
        plotting.plot_lcoe_flexibility_line_chart(cached_scenarios_list)
        plotting.plot_weekly_demand_curves(cached_scenarios_list)
        plotting.plot_weekly_operation_hours(cached_scenarios_list)

    else:
        # Create directory to cache this run
        cache_folder_name = str(datetime.datetime.now().strftime("%Y-%m-%d %Hh%M"))+' run_'+str(run_name)
        cache_folder_path = './deap_dsm_cache/' + cache_folder_name

        os.mkdir(cache_folder_path)
        os.mkdir(cache_folder_path + '/plots')
        os.mkdir(cache_folder_path + '/logbooks')

        # Copy UI_deap_dsm_config.py
        shutil.copy('UI_deap_dsm_config.py', './deap_dsm_cache/' + cache_folder_name)

        # Copy input data in cache folder
        shutil.copy('UI_user_input.py', './deap_dsm_cache/' + cache_folder_name)
        shutil.copy('./data/flow_rate.csv', './deap_dsm_cache/' + cache_folder_name)
        shutil.copy('./data/solar_potential.csv', './deap_dsm_cache/' + cache_folder_name)
        shutil.copy('./data/consumption_data', './deap_dsm_cache/' + cache_folder_name)
        shutil.copy('./data/working_hours_data', './deap_dsm_cache/' + cache_folder_name)

        # Initiate system components
        pv_systems = []
        for pv_nominal_power in UI_user_input.pv_steps:
            pv_systems.append(PvSystem(nominal_power=pv_nominal_power, solar_potential=solar_potential))

        mhps = []
        for mhp_nominal_flow_rate in UI_user_input.mhp_steps:
            mhps.append(Mhp(nominal_flow_rate=mhp_nominal_flow_rate, flow_rate_data=flow_rate))

        batteries = []
        for battery_capacity in UI_user_input.battery_steps:
            batteries.append(Battery(battery_capacity))

        # Create scenarios
        scenarios_list = []
        scenario_id = 0

        for mhp in mhps:
            for pv_system in pv_systems:
                for battery in batteries:
                    if pv_system.nominal_power > 0 and battery.max_soc == 0:
                        continue  # if pv_power > 0 battery capacity must be > 0
                    else:
                        # Create scenario as combination of capacities
                        scenarios_list.append(c_s.Scenario(scenario_id=scenario_id, mhp=mhp, pv_system=pv_system,
                                                           battery=battery, grid=Grid(), unscheduled_load_curve=
                                                           consumption_dict['unscheduled_load_curve']))
                        scenario_id = scenario_id + 1


        # ----- Run DEAP DSM -----
        for scenario in scenarios_list:

            t_deap_dsm_start = time.perf_counter()
            # -------- Run GA -----------

            population, hof_evolution, logbook = deap_run(
                optimization_start=consumers_data.scheduling_start,
                optimization_length=consumers_data.scheduling_end-consumers_data.scheduling_start,
                consumers_number=len(scheduled_consumers_list),
                job_dict=consumption_dict['job_dict'],
                power_availability=scenario.power_availability,
                working_hours_array=working_hours_array,
                cache_folder_path=cache_folder_path,
                scenario_id=scenario.scenario_id,
                battery=scenario.battery
            )

            scenario.hof_evolution = hof_evolution
            scenario.logbook = logbook

        # Pickle scenarios
        with open('./deap_dsm_cache/' + cache_folder_name + '/scenarios_list.pkl', 'wb') as output:
            pickle.dump(scenarios_list, output, pickle.HIGHEST_PROTOCOL)

    print("Total execution time: " + str(time.perf_counter()-t_total_start))
